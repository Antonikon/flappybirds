#pragma once

#include <QMainWindow>
#include <QtOpenGL/QGLWidget>
#include <QTimerEvent>
#include <QPainter>
#include <QMouseEvent>
#include <QKeyEvent>

#include "game.h"

class MainWindow : public QGLWidget
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    void timerEvent(QTimerEvent *event);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent* event);
    void resizeEvent(QResizeEvent *event);
    ~MainWindow();
private:
    TGame *Game = nullptr;
    int FpsTimerId;
    int Fps = 0;
    int FpsCount = 0;
};
