#pragma once

#include <QtCore>
#include <QPixmap>
#include <QPainter>
#include <QJsonDocument>
#include <QSettings>

#include <list>
#include <vector>

struct TBarrier {
    QRectF Geometry;
    bool Bottom = true;
    bool Scorable = false;
};

class TGame {
public:
    TGame(const QSize &screen);
    void SetScreen(const QSize &screen);
    void Draw(QPainter &painter);
    void Update();
    void AddBarrier(float x, float spaceY, float spaceHeight);
    void Jump();
    int GetObjectCount() const;
private:
    void Reset();
    void Die();
private:
    QSize WorldSize;
    float StartVelocity;
    float AccelerationPerScore;
    float BarrierDistance;
    float GapSize;
    float Gravity;
    float JumpForce;
    int MaxGapOffset;
    QSizeF PlayerSize;
    float Scale = 1;

    const QPixmap PlayerSpriteUp = QPixmap(":/data/player_up.png");
    const QPixmap PlayerSpriteDown = QPixmap(":/data/player_down.png");
    const QPixmap BarrierSprite = QPixmap(":/data/barrier.png");
    std::vector <std::pair <QPixmap, float> > Background;
    std::vector <float> BackgroundOffsets;

    float LastSpaceY;

    QSize Screen;

    float Offset = 0;
    std::list <TBarrier> Barriers;
    QRectF Player;
    float PlayerVelocity = 0;
    int Score = 0;
    int MaxScore = 0;
    bool GameOver = false;
    bool StartPause = true;

    int RestartTimer;

};
