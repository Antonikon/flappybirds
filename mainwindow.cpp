#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QGLWidget(parent)
{

#ifndef __ANDROID__
    setGeometry(QRect(50, 50, 1024, 576));
#endif

    QGLFormat format;
    format.setSwapInterval(0);
    format.setDoubleBuffer(true);

    setFormat(format);

    Game = new TGame(geometry().size());
    startTimer(1000 / 60.0);
    FpsTimerId = startTimer(1000);
}

void MainWindow::timerEvent(QTimerEvent *event) {
    if (event->timerId() == FpsTimerId) {
        Fps = FpsCount;
        FpsCount = 0;
    } else {
        if (Game) {
            Game->Update();
            update();
        }
    }
}

void MainWindow::paintEvent(QPaintEvent *event) {
    if (!Game) return;
    QPainter painter(this);
    Game->Draw(painter);
#ifndef QT_NO_DEBUG
    FpsCount++;
    painter.setPen(Qt::red);
    int objects = Game->GetObjectCount();
    painter.drawText(50, 50, 200, 200, Qt::AlignTop | Qt::AlignLeft, QString("FPS:%1\nOBJECTS:%2").arg(Fps).arg(objects));
#endif
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    Game->Jump();
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    Game->Jump();
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    Game->SetScreen(event->size());
}


MainWindow::~MainWindow()
{
}
