#include "game.h"

TGame::TGame(const QSize &screen) {
    QFile file(":/data/config.json");
    file.open(QFile::ReadOnly);
    QByteArray data = file.readAll();
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);
    file.close();

    if (error.error != QJsonParseError::ParseError::NoError) {
        qDebug() << error.errorString() << error.offset;
        throw std::runtime_error("Config invalid");
    }

    QJsonObject config = doc.object();

    QJsonArray worldSize = config["WorldSize"].toArray();
    WorldSize = QSize(worldSize[0].toInt(), worldSize[1].toInt());

    StartVelocity = config["StartVelocity"].toDouble();
    AccelerationPerScore = config["AccelerationPerScore"].toDouble();
    BarrierDistance = config["BarrierDistance"].toDouble();
    GapSize = config["GapSize"].toDouble();
    Gravity = config["Gravity"].toDouble();
    JumpForce = config["JumpForce"].toDouble();
    MaxGapOffset = config["MaxGapOffset"].toDouble();

    QJsonArray playerSize = config["PlayerBox"].toArray();
    PlayerSize = QSizeF(playerSize[0].toDouble(), playerSize[1].toDouble());
    if (PlayerSize.isNull()) PlayerSize = PlayerSpriteUp.size();

    QJsonArray background = config["Background"].toArray();
    Background.reserve(background.size());
    for (int i = 0; i < background.size(); i++) {
        QJsonArray bg = background[i].toArray();
        QPixmap img(":/data/" + bg[0].toString());
        if (img.isNull()) continue;
        float speedFactor = bg[1].toDouble();
        Background.push_back(std::make_pair(img, speedFactor));
    }

    QSettings settings("Example", "Flaapy Birds");
    MaxScore = settings.value("MaxScore", QVariant(0)).toInt();

    Reset();
    SetScreen(screen);
}

void TGame::SetScreen(const QSize &screen) {
    Screen = screen;
    Scale = (float)screen.height() / WorldSize.height();
}

void TGame::Draw(QPainter &painter) {
    painter.save();
    painter.fillRect(0, 0, Screen.width(), Screen.height(), Qt::black);

    for (int i = 0; i < Background.size(); i++) {
        const QPixmap &image = Background[i].first;
        QSize size = image.size();
        size *= Screen.height() / (float)size.height();
        float offset = BackgroundOffsets[i];
        while (offset < Screen.width()) {
            painter.drawPixmap(QRectF(offset, 0, size.width(), size.height()), image, QRectF(0, 0, image.width(), image.height()));
            offset += size.width();
        }
    }
    painter.save();
    painter.scale(Scale, Scale);
    painter.translate(-Offset, 0);

    std::vector <QPainter::PixmapFragment> fragmentList;
    fragmentList.reserve(Barriers.size());
    QPainter::PixmapFragment fragment;
    fragment.opacity = 1;
    fragment.scaleX = 1;
    fragment.scaleY = 1;
    fragment.width = BarrierSprite.width();
    fragment.height = BarrierSprite.height();
    fragment.sourceTop = 0;
    fragment.sourceLeft = 0;
    fragment.rotation = 0;
    for (const auto &barrier: Barriers) {
        fragment.scaleX = barrier.Geometry.width() / BarrierSprite.width() * (!barrier.Bottom ? -1 : 1);
        fragment.x = barrier.Geometry.center().x();
        fragment.rotation = 180 * !barrier.Bottom;
        if (barrier.Bottom) {
            fragment.y = barrier.Geometry.top() + BarrierSprite.height() / 2;
        } else {
            fragment.y = barrier.Geometry.bottom() - BarrierSprite.height() / 2;
        }
        fragmentList.push_back(fragment);
    }
    if (!fragmentList.empty()) {
        painter.drawPixmapFragments(fragmentList.data(), fragmentList.size(), BarrierSprite);
    }
    if (PlayerVelocity <= 0) {
        painter.drawPixmap(Player.center() - QPoint(PlayerSpriteUp.width(), PlayerSpriteUp.height()) / 2, PlayerSpriteUp);
    } else {
        painter.drawPixmap(Player.center() - QPoint(PlayerSpriteDown.width(), PlayerSpriteDown.height()) / 2, PlayerSpriteDown);
    }
    painter.restore();

    QFont font;
    font.setPixelSize(Screen.height() / 10);
    font.setBold(true);
    painter.setFont(font);
    painter.setPen(Qt::white);
    painter.drawText(10, 0, Screen.width() - 20, Screen.height(), Qt::AlignRight | Qt::AlignTop, QString::number(Score));
    painter.restore();

    if (GameOver) {
        QPainterPath text;
        float opacity = RestartTimer / (60.0 * 2);
        QPen pen;
        pen.setColor(Qt::black);
        pen.setWidthF(Scale);
        font.setPixelSize(Screen.height() / 5);

        text.addText(0, 0, font, "GAME OVER");
        QRectF box = text.boundingRect();
        text.translate(-box.left() + (Screen.width() - box.width()) / 2,
                       Screen.height() / 2 - box.bottom());

        painter.save();
        painter.setPen(pen);
        painter.setBrush(Qt::white);
        painter.setOpacity(opacity);
        painter.drawPath(text);

        std::vector <QString> lines = {
            QString("MAX SCORE: %1").arg(MaxScore),
            QString("SCORE: %1").arg(Score),
            QString("Press anywhere to restart")
        };

        if (RestartTimer % 120 < 60) lines.pop_back();

        int y = Screen.height() / 2 * 1.1;

        for (const auto &line: lines) {
            text = QPainterPath();
            font.setPixelSize(Screen.height() / 10);
            text.addText(0, 0, font, line);
            box = text.boundingRect();
            text.translate(-box.left() + (Screen.width() - box.width()) / 2,
                           y - box.top());

            painter.drawPath(text);
            y += box.height() * 1.25;

        }

        painter.drawRect(0, Screen.height() / 2 * 1.05, Screen.width(), Screen.height() / 2 * 0.025);

        painter.restore();
    }
}

void TGame::Update() {
    if (StartPause) return;

    if (!GameOver) {
        float velocity = StartVelocity + Score * AccelerationPerScore;

        for (int i = 0; i < Background.size(); i++) {
            const QPixmap &image = Background[i].first;
            float &offset = BackgroundOffsets[i];
            offset -= velocity * Background[i].second;

            QSizeF size = image.size();
            size *= Screen.height() / (float)size.height();
            while (offset <= -size.width()) {
                offset += size.width();
            }
        }

        Offset += velocity;
        Player.translate(velocity, 0);

        float right = 0;
        for (auto it = Barriers.begin(); it != Barriers.end();) {
            right = std::max(right, (float)it->Geometry.right());
            if (it->Geometry.right() < Player.left() && it->Scorable) {
                it->Scorable = false;
                Score++;
            }
            if (it->Geometry.right() < Offset) {
                it = Barriers.erase(it);
            } else it++;
        }


        if (Screen.width() / Scale + Offset - right >= BarrierDistance) {
            int top = std::max(LastSpaceY - MaxGapOffset, GapSize / 2);
            int bottom = std::min(LastSpaceY + MaxGapOffset, WorldSize.height() - GapSize / 2);

            LastSpaceY = top + rand() % (bottom - top);

            AddBarrier(Offset + Screen.width() / Scale, LastSpaceY, GapSize);
        }

        for (const auto &barrier: Barriers) {
            if (Player.intersects(barrier.Geometry)) {
                Die();
                break;
            }
        }
    }
    if (GameOver) {
        PlayerVelocity += Gravity;
        RestartTimer++;
        if (RestartTimer > 60 * 4) RestartTimer = 60 * 2;
    } else PlayerVelocity = std::min(PlayerVelocity + Gravity, JumpForce);

    if (Player.top() < WorldSize.height() * 2) Player.translate(0, PlayerVelocity);
    if (Player.top() < 0) {
        Player.translate(0, -Player.top());
        PlayerVelocity = 0;
    } else if (Player.top() > WorldSize.height() && !GameOver) Die();

}

void TGame::AddBarrier(float x, float spaceY, float spaceHeight) {
    QRectF top(x, 0, BarrierSprite.width(), spaceY - spaceHeight / 2);
    QRectF bottom(x, spaceY + spaceHeight / 2, BarrierSprite.width(),
                  WorldSize.height() - spaceY - spaceHeight / 2);
    TBarrier barrier;
    barrier.Geometry = top;
    barrier.Bottom = false;
    barrier.Scorable = false;
    Barriers.push_back(barrier);

    barrier.Geometry = bottom;
    barrier.Bottom = true;
    barrier.Scorable = true;
    Barriers.push_back(barrier);
}

void TGame::Jump() {
    if (GameOver) {
        if (RestartTimer >= 60 ) {
            Reset();
        }
        return;
    }
    if (StartPause) StartPause = false;
    PlayerVelocity = -JumpForce;
}

int TGame::GetObjectCount() const {
    return Barriers.size();
}

void TGame::Reset() {
    Offset = 0;
    Score = 0;
    GameOver = 0;
    PlayerVelocity = 0;
    Barriers.clear();
    LastSpaceY = WorldSize.height() / 2;
    Player = QRectF(QPointF(0, 0), PlayerSize);
    Player.translate(QPoint(WorldSize.width() / 5, WorldSize.height() / 2) - Player.center());
    GameOver = false;
    StartPause = true;
    BackgroundOffsets = std::vector <float>(Background.size(), 0);
    RestartTimer = true;
}

void TGame::Die() {
    PlayerVelocity = -JumpForce * 2/ 3;
    GameOver = true;
    RestartTimer = 0;
    MaxScore = std::max(Score, MaxScore);

    QSettings settings("Example", "Flaapy Birds");
    settings.setValue("MaxScore", MaxScore);
}
